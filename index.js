const express = require("express");

var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/Zigvy_NodeJS", function (error) {
    if (error) throw error;
    console.log('Successfully connected');
})

// Import file model
const userModel = require("./app/models/userModel");
const postModel = require("./app/models/postModel");
const commentModel = require("./app/models/commentModel");

// Import file router
const userRouter = require("./app/routers/userRouter");

const app = new express();

app.use(express.json());

app.use(express.urlencoded({
    urlencoded: true,
}));

const port = 8000;

app.use("/", userRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})