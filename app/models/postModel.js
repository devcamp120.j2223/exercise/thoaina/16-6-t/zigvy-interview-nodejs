// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const postSchema = new Schema({
    _postId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
});

// export 
module.exports = mongoose.model("Post", postSchema);