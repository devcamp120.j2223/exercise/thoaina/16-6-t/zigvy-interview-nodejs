// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const userSchema = new Schema({
    _userId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
});

// export 
module.exports = mongoose.model("User", userSchema);