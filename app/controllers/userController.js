// import user model
const { mongoose } = require("mongoose");
const userModel = require("../models/userModel");

// get all users
const getAllUser = (req, res) => {
    console.log("Get all users");
    res.json({
        message: "Get all users",
    })
};

const getUserById = (req, res) => {
    let id = req.params.userId;
    console.log("Get userId: " + id);
    res.json({
        message: "Get userId " + id,
    })
};

// tạo mới User
const createUser = (req, res) => {
    let body = req.body;
    console.log("Create a user");
    console.log(body);
    res.json({
        ...body,
    })
};

const updateUserById = (req, res) => {
    let id = req.params.userId;
    let body = req.body;
    console.log("Get userId: " + id);
    res.json({
        message: { id, ...body },
    })
};

const deleteUserById = (req, res) => {
    let id = req.params.userId;
    console.log("Delete a user " + id);
    res.json({
        message: "delete a user " + id,
    })
};
// export các hàm
module.exports = { getAllUser, getUserById, createUser, updateUserById, deleteUserById };